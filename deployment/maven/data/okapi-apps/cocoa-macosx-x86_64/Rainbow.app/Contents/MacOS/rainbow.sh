#!/bin/bash

JAVA="$(/usr/libexec/java_home -v 1.7+)/bin/java"

cd "$(dirname "$0")"
"$JAVA" -d64 -XstartOnFirstThread -Xdock:name="Rainbow" -jar ../../../lib/rainbow.jar
