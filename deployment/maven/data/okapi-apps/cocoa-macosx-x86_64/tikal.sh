#!/bin/bash

JAVA="$(/usr/libexec/java_home -v 1.7+)/bin/java"

"$JAVA" -d64 -XstartOnFirstThread -jar "$(dirname "$0")/lib/tikal.jar" "$@"
