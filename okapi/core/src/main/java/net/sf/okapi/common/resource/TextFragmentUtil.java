package net.sf.okapi.common.resource;

import java.util.Set;
import java.util.TreeSet;

public final class TextFragmentUtil {

	/**
	 * Store the missing, added or modified codes in the target as compared to
	 * the source.
	 * <p>
	 * To assure consistent ids and code data run
	 * {@link TextFragment#alignCodeIds(TextFragment)} first.
	 * 
	 * @param source
	 *            - source {@link TextFragment}, use as the standard to compare
	 *            against.
	 * @param target
	 *            - target {@link TextFragment} to compare codes with source.
	 * @return {@link CodeAnomalies} or null if no anomalies.
	 */
	static public CodeAnomalies catalogCodeAnomalies(TextFragment source, TextFragment target) {
		return catalogCodeAnomalies(source, target, true);
	}
	
	/**
	 * Store the missing oradded codes in the target as compared to
	 * the source.
	 * <p>
	 * To assure consistent ids and code data run
	 * {@link TextFragment#alignCodeIds(TextFragment)} first.
	 * 
	 * @param source
	 *            - source {@link TextFragment}, use as the standard to compare
	 *            against.
	 * @param target
	 *            - target {@link TextFragment} to compare codes with source.
	 * @param includeDeletable - do we count deletable codes as missing? True by default.            
	 * @return {@link CodeAnomalies} or null if no anomalies.
	 */
	static public CodeAnomalies catalogCodeAnomalies(TextFragment source, TextFragment target, 
			boolean includeDeletable) {
		CodeAnomalies anomalies = new CodeAnomalies();

		// both are null, no anomalies
		if (source == null && target == null) {
			return null;
		}

		if (source == null) {
			// target isn't null
			if (!target.hasCode()) {
				return null;
			}
			for (Code c : target.getCodes()) {
				anomalies.addAddedCode(c);
				return anomalies;
			}
		}

		if (target == null) {
			// source isn't null
			if (!source.hasCode()) {
				return null;
			}
			for (Code c : source.getCodes()) {
				anomalies.addMissingCode(c);
				return anomalies;
			}
		}
		
		// remaining codes in targetCodeSet are not found in the source
		Set<Code> sourceCodeSet = new TreeSet<>(new CodeComparatorOnData());
		Set<Code> targetCodeSet = new TreeSet<>(new CodeComparatorOnData());
		if (source != null) {
			sourceCodeSet.addAll(source.getCodes());
		}
		
		if (target != null) {
			targetCodeSet.addAll(target.getCodes());
		}
		
		targetCodeSet.removeAll(sourceCodeSet);
		for (Code c : targetCodeSet) {
			if (c.isDeleteable() && !includeDeletable) {
				continue;
			} 
			anomalies.addAddedCode(c);
		}
		
		// remaining codes in sourceCodeSet are not found in the target
		if (target != null) {
			targetCodeSet.addAll(target.getCodes());
		}
		sourceCodeSet.removeAll(targetCodeSet);
		for (Code c : sourceCodeSet) {
			if (c.isDeleteable() && !includeDeletable) {
				continue;
			} 
			anomalies.addMissingCode(c);
		}

		if (anomalies.hasAddedCodes() || anomalies.hasMissingCodes()) {
			return anomalies;
		}
		
		return null;
	}
}
