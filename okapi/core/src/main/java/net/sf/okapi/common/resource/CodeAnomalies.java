/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.common.resource;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Container class for each type of possible code anomaly. 
 * Normal use case is missing or added codes in a target 
 * {@link TextFragment} as compared to its source equivalent.
 * More list types can be added if needed.
 * <p>
 * Returned by {@link TextFragment#alignCodeIds(TextFragment)}
 * @author jimh
 */
public class CodeAnomalies {
	private List<Code> addedCodes;
	private List<Code> missingCodes;

	public CodeAnomalies() {
		addedCodes = new LinkedList<>();
		missingCodes = new LinkedList<>();
	}
	
	public void addMissingCode(Code code) {
		// we don't care if its only an annotation
		if (code.hasOnlyAnnotation()) return;
		missingCodes.add(code);
	}
	
	public void addAddedCode(Code code) {
		// we don't care if its only an annotation
		if (code.hasOnlyAnnotation()) return;
		addedCodes.add(code);
	}
		
	public boolean hasMissingCodes() {
		return !missingCodes.isEmpty();
	}
	
	public boolean hasAddedCodes() {
		return !addedCodes.isEmpty();
	}
	
	public Iterator<Code> getMissingCodesIterator() {
		return missingCodes.listIterator();
	}
	
	public Iterator<Code> getAddedCodesIterator() {
		return addedCodes.listIterator();
	}
	
	public String addedCodesAsString() {
		StringBuffer b = new StringBuffer();
		for (Code c : addedCodes) {
			b.append(c.getData());
			b.append(',');
		}
		if (b.length() > 0) {
			// remove last comma
			b.deleteCharAt(b.length()-1);
		}
		return b.toString();
	}
	
	public String missingCodesAsString() {
		StringBuffer b = new StringBuffer();
		for (Code c : missingCodes) {
			b.append(c.getData());
			b.append(',');
		}
		if (b.length() > 0) {
			// remove last comma
			b.deleteCharAt(b.length()-1);
		}
		return b.toString();
	}
}
