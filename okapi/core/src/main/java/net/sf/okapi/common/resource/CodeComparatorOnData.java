package net.sf.okapi.common.resource;

import java.util.Comparator;


public class CodeComparatorOnData implements Comparator<Code> {

	@Override
	public int compare(Code c1, Code c2) {	  
		if (c1.getData().equals(c2.getData())) {
			// if tag string is the same then look at ids to sort on them
			// must run alignCodes for this to work reliably
			// this allows us to handle duplicate codes in TreeSet
			return Integer.compare(c1.getId(), c2.getId());
		} else {
			return c1.getData().compareTo(c2.getData());
		}
	}
}
