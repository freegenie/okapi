/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.inconsistencycheck;

class Duplicate {

    private String text;
    private String display;
    private String docId;
    private String subDocId;
    private String tuId;
    private String segId;

    public Duplicate(String docId, String subDocId, String tuId, String segId, String text, String display) {
        this.display = display;
        this.text = text;
        this.docId = docId;
        this.subDocId = subDocId;
        this.tuId = tuId;
        this.segId = segId;
    }

    public String getText() {
        return text;
    }

    public String getDisplay() {
        return display;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getSubDocId() {
        return subDocId;
    }

    public void setSubDocId(String subDocId) {
        this.subDocId = subDocId;
    }

    public String getTuId() {
        return tuId;
    }

    public void setTuId(String tuId) {
        this.tuId = tuId;
    }

    public String getSegId() {
        return segId;
    }

    public void setSegId(String segId) {
        this.segId = segId;
    }
}
