/*===========================================================================
 Copyright (C) 2010-2013 by the Okapi Framework contributors
 -----------------------------------------------------------------------------
 This library is free software; you can redistribute it and/or modify it 
 under the terms of the GNU Lesser General Public License as published by 
 the Free Software Foundation; either version 2.1 of the License, or (at 
 your option) any later version.

 This library is distributed in the hope that it will be useful, but 
 WITHOUT ANY WARRANTY; without even the implied warranty of 
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License 
 along with this library; if not, write to the Free Software Foundation, 
 Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
 ===========================================================================*/

package net.sf.okapi.steps.copyormove;

import java.io.File;
import java.net.URI;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.StreamUtil;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;

public class CopyOrMoveStep extends BasePipelineStep {

	private Parameters params;
	private URI outputURI;

	public CopyOrMoveStep() {
		reset();
	}

	@Override
	public String getName() {
		return "Copy Or Move";
	}

	@Override
	public String getDescription() {
		return "Copies or moves the listed files to the specified location. "
				+ "Expects: raw documents. Sends back: raw documents.";
	}

	@Override
	public IParameters getParameters() {
		return params;
	}

	@Override
	public void setParameters(IParameters params) {
		this.params = (Parameters) params;
	}

	@StepParameterMapping(parameterType = StepParameterType.OUTPUT_URI)
	public void setOutputURI(URI outputURI) {
		this.outputURI = outputURI;
	}

	@Override
	public Event handleRawDocument (Event event) {
		File file = new File(event.getRawDocument().getInputURI());
		File output = new File(outputURI);

		if ( params.getCopyOption().equals("overwrite") ) {
			StreamUtil.copy(file.getPath(), output.getPath(), params.isMove());
		}
		else if ( params.getCopyOption().equals("backup") ) {
			if ( output.exists() ) {
				if ( !output.renameTo(new File(outputURI.toString() + ".bak")) ) {
					StreamUtil.copy(output.getPath(), output.getPath().replace(".txt", ".txt.bak"), true);
				}
			}
			StreamUtil.copy(file.getPath(), output.getPath(), params.isMove());
		}
		else { // skip copy/move file
			if ( !output.exists() ) {
				StreamUtil.copy(file.getPath(), output.getPath(), params.isMove());
			}
		}
		return event;
	}

	private void reset() {
		this.params = new Parameters();
	}
}
