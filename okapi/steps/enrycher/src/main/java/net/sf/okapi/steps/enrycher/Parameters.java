/*===========================================================================
  Copyright (C) 2012 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.enrycher;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.SpinInputPart;

@EditorFor(Parameters.class)
public class Parameters extends StringParameters implements IEditorDescriptionProvider {
	
	private static final String BASEURL = "baseUrl";
	private static final String MAXEVENTS = "maxEvents";
	
	public Parameters() {
		super();
	}
	
	@Override
	public void reset() {
		super.reset();
		setBaseUrl("http://aidemo.ijs.si/mlw");
		setMaxEvents(20);
	}
	
	public String getBaseUrl () {
		return getString(BASEURL);
	}
	
	public void setBaseUrl (String baseUrl) {
		setString(BASEURL, Util.ensureSeparator(baseUrl, true));
	}

	public int getMaxEvents () {
		return getInteger(MAXEVENTS);
	}
	
	public void setMaxEvents (int maxEvents) {
		setInteger(MAXEVENTS, maxEvents);
	}
	
	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(BASEURL, "URL of the Enrycher Web service", null);
		desc.add(MAXEVENTS, "Events buffer", "Number of events to store before sending a query");
		return desc;
	}
	
	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("Enrycher", true, false);
		desc.addTextInputPart(paramsDesc.get(BASEURL));
		SpinInputPart sip = desc.addSpinInputPart(paramsDesc.get(MAXEVENTS));
		sip.setRange(1, 999);
		return desc;
	}
	
}
