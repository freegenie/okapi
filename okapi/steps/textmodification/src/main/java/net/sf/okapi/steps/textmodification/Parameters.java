/*===========================================================================
  Copyright (C) 2008-2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.textmodification;

import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {
	
	public static final int TYPE_KEEPORIGINAL = 0;
	public static final int TYPE_XNREPLACE = 1;
	public static final int TYPE_KEEPINLINE = 2;
	public static final int TYPE_EXTREPLACE = 3;
	
	private static final String APPLYTOBLANKENTRIES = "applyToBlankEntries";
	private static final String EXPAND = "expand";
	private static final String SCRIPT = "script";
	private static final String TYPE = "type";
	private static final String ADDPREFIX = "addPrefix";
	private static final String PREFIX = "prefix";
	private static final String ADDSUFFIX = "addSuffix";
	private static final String SUFFIX = "suffix";
	private static final String APPLYTOEXISTINGTARGET = "applyToExistingTarget";
	private static final String ADDNAME = "addName";
	private static final String ADDID = "addID";
	private static final String MARKSEGMENTS = "markSegments";

	public Parameters () {
		super();
	}
	
	public int getType() {
		return getInteger(TYPE);
	}

	public void setType(int type) {
		setInteger(TYPE, type);
	}

	public boolean getAddPrefix() {
		return getBoolean(ADDPREFIX);
	}

	public void setAddPrefix(boolean addPrefix) {
		setBoolean(ADDPREFIX, addPrefix);
	}

	public String getPrefix() {
		return getString(PREFIX);
	}

	public void setPrefix(String prefix) {
		setString(PREFIX, prefix);
	}

	public boolean getAddSuffix() {
		return getBoolean(ADDSUFFIX);
	}

	public void setAddSuffix(boolean addSuffix) {
		setBoolean(ADDSUFFIX, addSuffix);
	}

	public String getSuffix() {
		return getString(SUFFIX);
	}

	public void setSuffix(String suffix) {
		setString(SUFFIX, suffix);
	}

	public boolean getApplyToExistingTarget() {
		return getBoolean(APPLYTOEXISTINGTARGET);
	}

	public void setApplyToExistingTarget(boolean applyToExistingTarget) {
		setBoolean(APPLYTOEXISTINGTARGET, applyToExistingTarget);
	}

	public boolean getAddName() {
		return getBoolean(ADDNAME);
	}

	public void setAddName(boolean addName) {
		setBoolean(ADDNAME, addName);
	}

	public boolean getAddID() {
		return getBoolean(ADDID);
	}

	public void setAddID(boolean addID) {
		setBoolean(ADDID, addID);
	}

	public boolean getMarkSegments() {
		return getBoolean(MARKSEGMENTS);
	}

	public void setMarkSegments(boolean markSegments) {
		setBoolean(MARKSEGMENTS, markSegments);
	}

	public boolean getApplyToBlankEntries() {
		return getBoolean(APPLYTOBLANKENTRIES);
	}

	public void setApplyToBlankEntries(boolean applyToBlankEntries) {
		setBoolean(APPLYTOBLANKENTRIES, applyToBlankEntries);
	}

	public boolean getExpand() {
		return getBoolean(EXPAND);
	}

	public void setExpand(boolean expand) {
		setBoolean(EXPAND, expand);
	}

	public int getScript() {
		return getInteger(SCRIPT);
	}

	public void setScript(int script) {
		setInteger(SCRIPT, script);
	}
	
	public void reset() {
		super.reset();
		setType(0);
		setAddPrefix(false);
		setPrefix("{START_");
		setAddSuffix(false);
		setSuffix("_END}");
		setApplyToExistingTarget(false);
		setAddName(false);
		setAddID(false);
		setMarkSegments(false);
		setApplyToBlankEntries(true); // For backward compatibility
		setExpand(false);
		setScript(0);
	}
}
