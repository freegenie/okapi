/*===========================================================================
  Copyright (C) 2011 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.repetitionanalysis;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.SpinInputPart;

@EditorFor(Parameters.class)
public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String FUZZYTHRESHOLD = "fuzzyThreshold";
	private static final String MAXHITS = "maxHits";
	
	public Parameters () {
		super();
	}
	
	public void reset() {
		super.reset();
		setFuzzyThreshold(100);
		setMaxHits(20);
	}
	
	public void setFuzzyThreshold(int fuzzyThreshold) {
		setInteger(FUZZYTHRESHOLD, fuzzyThreshold);
	}

	public int getFuzzyThreshold() {
		return getInteger(FUZZYTHRESHOLD);
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(FUZZYTHRESHOLD, "Fuzzy threshold (1-100)", "Fuzzy threshold for fuzzy repetitions. Leave 100 for exact repetitions only.");
		desc.add(MAXHITS, "Max hits", "Maximum number of exact and fuzzy repetitions to keep track of for every segment.");
		return desc;
	}
	
	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramDesc) {
		EditorDescription desc = new EditorDescription("Repetition Analysis");		
		SpinInputPart sip = desc.addSpinInputPart(paramDesc.get(FUZZYTHRESHOLD));
		SpinInputPart sip2 = desc.addSpinInputPart(paramDesc.get(MAXHITS));
		sip.setRange(1, 100);		
		sip2.setRange(1, 100);
		return desc;
	}

	public int getMaxHits() {
		return getInteger(MAXHITS);
	}

	public void setMaxHits(int maxHits) {
		setInteger(MAXHITS, maxHits);
	}

}
