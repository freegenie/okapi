/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.ttxsplitter;

import java.net.URI;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.exceptions.OkapiBadStepInputException;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.resource.RawDocument;

@UsingParameters(TTXSplitterParameters.class)
public class TTXSplitterStep extends BasePipelineStep {

	private TTXSplitterParameters params;
//	private boolean done = false;
	private TTXSplitter splitter;
	
	public TTXSplitterStep () {
		params = new TTXSplitterParameters();
	}

	@Override
	public String getDescription () {
		return "Splits a TTX document into several ones of roughly equal word count. "
			+ "Expects: raw document. Sends back: raw document.";
	}

	@Override
	public String getName() {
		return "TTX Splitter";
	}

	@Override
	public IParameters getParameters () {
		return params;
	}

	@Override
	public void setParameters (final IParameters params) {
		this.params = (TTXSplitterParameters)params;
	}

	@Override
	protected Event handleStartBatch (final Event event) {
		splitter = new TTXSplitter(params);
//		done = true;
		return event;
	}

	@Override
	protected Event handleStartBatchItem (final Event event) {
//		done = false;
		return event;
	}

//	@Override
//	public boolean isDone () {
//		return done;
//	}

	@Override
	protected Event handleRawDocument (final Event event) {
		final RawDocument rawDoc = event.getRawDocument();
		//TODO: change this to support input stream instead
		URI uri = rawDoc.getInputURI();
		if ( uri == null ) {
			throw new OkapiBadStepInputException("TTX Splitter expects a URI input.");
		}
		splitter.split(uri);
		return event;
	}

}
