/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.ttxsplitter;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.SpinInputPart;

@EditorFor(TTXSplitterParameters.class)
public class TTXSplitterParameters extends StringParameters implements IEditorDescriptionProvider {

	private static final String PARTCOUNT = "partCount";

	public TTXSplitterParameters() {
		super();
	}

	public void reset() {
		super.reset();
		setPartCount(2);
	}

	public int getPartCount () {
		return getInteger(PARTCOUNT);
	}
	
	public void setPartCount (int partCount) {
		setInteger(PARTCOUNT, partCount);
	}

	@Override
	public ParametersDescription getParametersDescription() {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(PARTCOUNT, "Number of output files", null);
		return desc;
	}
	
	@Override
	public EditorDescription createEditorDescription (ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("TTX Splitter", true, false);
		SpinInputPart sip = desc.addSpinInputPart(paramsDesc.get(PARTCOUNT));
		sip.setRange(2, 999);
		return desc;
	}

}