/*===========================================================================
  Copyright (C) 2008-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.xliffkit.codec;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.okapi.common.RegexUtil;

public class PackageSymCodec extends BasePackageCodec {

	private static final String[] LOOKUP = new String[] {
		"NUL", "SOH", "STX", "ETX", "EOT", "ENQ", "ACK", "BEL", "BS", "HT", "LF", "VT",
		"FF", "CR", "SO", "SI", "DLE", "DC1", "DC2", "DC3", "DC4", "NAK", "SYN", "ETB",
		"CAN", "EM", "SUB", "ESC", "FS", "GS", "RS", "US", "DEL"
	};

	private static final String MASK = "[%s]";
	
	private static Pattern[] patterns;
	private String mask;

	public PackageSymCodec() {
		this(MASK);		
	}
	
	public PackageSymCodec(String mask) {
		super();
		this.mask = mask;
		patterns = new Pattern[LOOKUP.length];
		for (int i = 0; i < LOOKUP.length; i++) {
			String st = String.format(mask, LOOKUP[i]);
			patterns[i] = Pattern.compile(RegexUtil.escape(st));
		}		
	}
	
	@Override
	protected String doEncode(int codePoint) {
		if (codePoint == 0x7F) codePoint = 0x20;		
		if (codePoint <= 0x20) {
			String st = String.format(mask, LOOKUP[codePoint]);
			return st;
		}
		else
			return new String(Character.toChars(codePoint));
	}

	@Override
	protected String doDecode(String text) {
		for (int i = 0; i < patterns.length; i++) {
			Pattern pattern = patterns[i];
			Matcher matcher = pattern.matcher(text);
			if (!matcher.find()) continue;
			
			String replacement = getReplacement(i);
			text = pattern.matcher(text).replaceAll(replacement);
		}
		return text;
	}

	private String getReplacement(int index) {
		if (index == 0x20) index = 0x7F;
		return new String(Character.toChars(index));
	}
}
