package net.sf.okapi.tkit.integration.common;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import net.sf.okapi.common.ClassUtil;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.FileUtil;
import net.sf.okapi.common.ListUtil;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.StreamUtil;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.XMLFileCompare;
import net.sf.okapi.common.ZipFileCompare;
import net.sf.okapi.common.ZipXMLFileCompare;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filters.InputDocument;
import net.sf.okapi.common.pipeline.IPipelineStep;
import net.sf.okapi.filters.html.HtmlFilter;
import net.sf.okapi.filters.html.HtmlSnippetsTest;
import net.sf.okapi.filters.idml.IDMLFilter;
import net.sf.okapi.filters.idml.tests.IDMLFilterTest;
import net.sf.okapi.filters.json.JSONFilter;
import net.sf.okapi.filters.json.JSONFilterTest;
import net.sf.okapi.filters.openxml.OpenXMLFilter;
import net.sf.okapi.filters.openxml.OpenXMLTest;
import net.sf.okapi.filters.plaintext.PlainTextFilter;
import net.sf.okapi.filters.plaintext.PlainTextFilterTest;
import net.sf.okapi.filters.po.POFilter;
import net.sf.okapi.filters.po.POFilterTest;
import net.sf.okapi.filters.properties.PropertiesFilter;
import net.sf.okapi.filters.properties.PropertiesFilterTest;
import net.sf.okapi.filters.regex.RegexFilter;
import net.sf.okapi.filters.regex.RegexFilterTest;
import net.sf.okapi.filters.table.CommaSeparatedValuesFilterTest;
import net.sf.okapi.filters.table.TableFilter;
import net.sf.okapi.filters.table.TableFilterTest;
import net.sf.okapi.filters.table.base.Parameters;
import net.sf.okapi.filters.ts.TsFilter;
import net.sf.okapi.filters.ts.TsFilterTest;
import net.sf.okapi.filters.xliff.XLIFFFilter;
import net.sf.okapi.filters.xliff.XLIFFFilterTest;
import net.sf.okapi.filters.xml.XMLFilter;
import net.sf.okapi.filters.xml.XMLFilterTest;
import net.sf.okapi.filters.xmlstream.XmlStreamFilter;
import net.sf.okapi.filters.xmlstream.integration.CdataSubfilterWithRegexTest;
import net.sf.okapi.filters.xmlstream.integration.DitaExtractionComparisionTest;
import net.sf.okapi.filters.xmlstream.integration.PIExtractionTest;
import net.sf.okapi.filters.xmlstream.integration.PropertyXmlExtractionComparisionTest;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatch;
import net.sf.okapi.lib.extra.pipelinebuilder.XBatchItem;
import net.sf.okapi.lib.extra.pipelinebuilder.XPipeline;
import net.sf.okapi.lib.extra.steps.CompoundStep;
import net.sf.okapi.lib.extra.steps.TuDpSgLogger;
import net.sf.okapi.steps.common.FilterEventsToRawDocumentStep;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DoubleExtractionIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	protected LocaleId locEN = LocaleId.fromString("en");
	protected LocaleId locFR = LocaleId.fromString("fr");
	protected LocaleId locFRCA = LocaleId.fromString("fr-ca");
	protected LocaleId locES = LocaleId.fromString("es");
	protected LocaleId locDE = LocaleId.fromString("de");
	
	private String paramFile;

//	For the tests be able of using test resources in other modules, use the following dependencies in the pom 
//	(see http://stackoverflow.com/questions/174560/sharing-test-code-in-maven):
//	
//	<dependency>
//		<groupId>net.sf.okapi.filters</groupId>
//		<artifactId>okapi-filter-table</artifactId>
//		<version>${project.version}</version>
//		<type>test-jar</type>
//		<scope>test</scope>
//	</dependency>

	@Test
	public void testHtmlFilter () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new HtmlFilter();
		run(testFilter(filter, HtmlSnippetsTest.class, "324.html", null,
				"UTF-8", locEN, locFR),			
			testFilter(filter, HtmlSnippetsTest.class, "ugly_big.htm", null,
				"UTF-8", locEN, locFR));
	}
	
	@Test
	public void testHtmlFilter2 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new HtmlFilter();
		run(testFilter(filter, HtmlSnippetsTest.class, "burlington_ufo_center.html", null,
				"UTF-8", locEN, locFR, true, true));
	}
	
	@Ignore("This test was an attempt to process non-wellformed html files with the wellformed config. The attempt fails.")
	@Test
	public void testHtmlFilter3 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new HtmlFilter();
		run(testFilter(filter, HtmlSnippetsTest.class, "324.html", "wellformedConfiguration.yml",
				"UTF-8", locEN, locFR, true, false //,
				// These files fail with the wellformed config
//				"sanitizer.html", 
//				"mandrake_fonts.html",
//				"okapi_intro_test.html",				
//				"test.html",
//				"324.html",
//				"form.html", // passes, but creates invalid XLIFF
//				"home_big.html", // passes, but creates invalid XLIFF
//				"home_crush.html", // passes, but creates invalid XLIFF
//				"home_tagcloud_hell.html",
//				"msg00058.html"
				), 			
			testFilter(filter, HtmlSnippetsTest.class, "ugly_big.htm", "wellformedConfiguration.yml",
				"UTF-8", locEN, locFR, true, false //,
//				// These files fail with the wellformed config
//				"home.htm",
//				"ugly_big.htm",
//				"We The People Foundation.htm",
//				"World'sWorstWebsite.htm",
//				"TeacherXpress.htm",
//				"BadTags_HTMLDog.htm",
//				"d_Lux_MediaArts.htm",
//				"Carnation Chiropractic Center Inc.htm"
				));
	}
	
	@Test
	public void testHtmlFilter4 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new HtmlFilter();
		run(testFilter(filter, HtmlSnippetsTest.class, "324.html", "nonwellformedConfiguration.yml",
				"UTF-8", locEN, locFR),			
			testFilter(filter, HtmlSnippetsTest.class, "ugly_big.htm", "nonwellformedConfiguration.yml",
				"UTF-8", locEN, locFR));
	}
	
	@Test
	public void testHtmlFilter5 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new HtmlFilter();
		run(testFilter(filter, HtmlSnippetsTest.class, "FFFEBOM.html", null,
				"UTF-8", locEN, locFR, true, true));
	}
	
	@Test
	public void testHtmlFilter6 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new HtmlFilter();
		run(testFilter(filter, HtmlSnippetsTest.class, "home_big.html", "nonwellformedConfiguration.yml",
				"UTF-8", locEN, locFR, true, true));
	}
	
	@Test
	public void testHtmlFilter7 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new HtmlFilter();
		run(testFilter(filter, HtmlSnippetsTest.class, "form.html", "nonwellformedConfiguration.yml",
				"UTF-8", locEN, locFR, true, true));
	}
	
	@Test
	public void testIdmlFilter () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new IDMLFilter();
		run(zipXmlTestFilter(filter, IDMLFilterTest.class, "ConditionalText.idml", "/okf_idml@ExtractAll.fprm", 
				locEN, locFR, false)
				);
	}
	
	@Test
	public void testTsFilter () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new TsFilter();
		run(testFilter(filter, TsFilterTest.class, "alarm_ro.ts", null, 
				"UTF-8", locEN, locFR)
				);
	}
	
	@Test
	public void testTsFilter2 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new TsFilter();
		run(testFilter(filter, TsFilterTest.class, "alarm_ro.ts", null, 
				"UTF-8", locEN, locFR, true, true)
				);
	}
	
	@Test
	public void testTsFilter3 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new TsFilter();
		run(testFilter(filter, TsFilterTest.class, "Test_nautilus.af.ts", null, 
				"UTF-8", locEN, locFR, true, true)
				);
	}	
	
	@Test
	public void testJsonFilter () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new JSONFilter();
		run(testFilter(filter, JSONFilterTest.class, "test01.json", null, 
				"UTF-8", locEN, locFR)
				);
	}
	
	@Test
	public void testJsonFilter2 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new JSONFilter();
		run(testFilter(filter, JSONFilterTest.class, "test07-subfilter.json", null, 
				"UTF-8", locEN, locFR, true, true)
				);
	}
	
	@Test
	public void testOpenXmlFilter () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new OpenXMLFilter();
		
		run(zipTestFilter(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, new String[] {			
			"Addcomments.docx",
			"AlternateContentTest.docx",
			"BoldWorld.docx",
			"Deli.docx",
			"DocProperties.docx",
			"docxsegtest.docx",
			"docxtest.docx",
			"EndGroup.docx",
			"equation.docx",
			"Escapades.docx",
			"Hangs.docx",
			"Hidden.docx",
			"Mauris.docx",
			"MissingPara.docx",
			"neverendingloop.docx",
			"OutOfTheTextBox.docx",
			"Practice2.docx",
			"reordered-zip.docx",
			"sample.docx",
			"SampleRuby.docx",
			"shape with text.docx",
			"styles.docx",
			"table of contents - automatic.docx",
			"TestDako2.docx",
			"TestLTinsideBoxFails.docx",
			"TextBoxes.docx",
			"UTF8.docx",
			"watermark.docx",
			"word art.docx"
			}, null, "UTF-8", locEN, locFR, true
				),
//		run(zipTestFilter(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, "gold/OutAddcomments.docx", null, 
//						"UTF-8", locEN, locFR
//						),				
				
			zipTestFilter(loadFilter(filter, "excelConfiguration.yml"), OpenXMLTest.class, "ExcelColors.xlsx", null, 
				"UTF-8", locEN, locFR
				),
		
			zipTestFilter(loadFilter(filter, "powerpointConfiguration.yml"), OpenXMLTest.class, "InsertText.pptx", null, 
				"UTF-8", locEN, locFR)
				);
	}
	
	@Test
	public void testOpenXmlFilter2 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new OpenXMLFilter();
		
		run(zipTestFilter(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, new String[] {			
			"EndGroup.docx",
			}, null, "UTF-8", locEN, locFR, true)
		);
	}
	
	@Test
	public void testOpenXmlFilter3 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new OpenXMLFilter();
		
		run(zipTestFilter(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, new String[] {			
			"AlternateContentTest.docx",
			}, null, "UTF-8", locEN, locFR, true)
		);
	}
	
	@Test
	public void testOpenXmlFilter6 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new OpenXMLFilter();
		
		run(zipTestFilter(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, new String[] {			
			"TestDako2.docx",
			}, null, "UTF-8", locEN, locFR, true)
		);
	}
	
	@Test
	public void listEvents_OpenXmlFilter () throws URISyntaxException {
		IFilter filter = new OpenXMLFilter();
        
//        listZipEvents(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, "UTF-8", locEN, locFR, "sample.docx");
        listEvents(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, "UTF-8", locEN, locFR, "sample.docx");
	}
	
	@Test
	public void listEvents_OpenXmlFilter2 () throws InstantiationException, IllegalAccessException, URISyntaxException {
		IFilter filter = new OpenXMLFilter();
		
//		listZipEvents(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, "UTF-8", locEN, locFR,			
//			"EndGroup.docx");
		listEvents(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, "UTF-8", locEN, locFR,			
				"EndGroup.docx");
	}
	
	@Test
	public void listEvents_OpenXmlFilter3 () throws InstantiationException, IllegalAccessException, URISyntaxException {
		IFilter filter = new OpenXMLFilter();
		
		listZipEvents(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, "UTF-8", locEN, locFR,			
			"EndGroup.docx");
	}
	
	@Test
	public void listEvents_OpenXmlFilter4 () throws URISyntaxException {
		IFilter filter = new OpenXMLFilter();
        
		// Testing when there is an actual fe2rd on the pipeline, if no conflict with RSS 
        listZipEvents(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, "UTF-8", locEN, locFR, "sample.docx");
	}
	
	@Test
	public void listEvents_OpenXmlFilter5 () throws InstantiationException, IllegalAccessException, URISyntaxException {
		IFilter filter = new OpenXMLFilter();
		
		listZipEvents(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, "UTF-8", locEN, locFR,			
			"AlternateContentTest.docx");
	}
	
	@Test
	public void listEvents_OpenXmlFilter6 () throws InstantiationException, IllegalAccessException, URISyntaxException {
		IFilter filter = new OpenXMLFilter();
		
		listZipEvents(loadFilter(filter, "wordConfiguration.yml"), OpenXMLTest.class, "UTF-8", locEN, locFR,			
				"TestDako2.docx");
	}
	
	@Test
	public void testPlaintextFilter () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new PlainTextFilter();
		
		run(testFilter(loadFilter(filter, "okf_plaintext.fprm"), PlainTextFilterTest.class, "BOM_MacUTF16withBOM2.txt", null, 
				"UTF-8", locEN, locFR),
			testFilter(loadFilter(filter, "/test_params1.fprm"), PlainTextFilterTest.class, "test_params1.txt", null, 
					"UTF-8", locEN, locFR, true, true),
			testFilter(loadFilter(filter, "/test_params2.fprm"), PlainTextFilterTest.class, "test_params2.txt", null, 
					"UTF-8", locEN, locFR, true, true)
			);
	}
	
	@Test // Added/Missing code exception
	public void testPoFilter () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new POFilter();
		run(testFilter(filter, POFilterTest.class, "AllCasesTest.po", null, 
			"UTF-8", locEN, locFR),			
			testFilter(loadFilter(filter, "/okf_po@Monolingual.fprm"), POFilterTest.class, "TestMonoLingual_EN.po", null, 
					"UTF-8", locEN, locFR, true, true),
			testFilter(loadFilter(filter, "/okf_po@Monolingual.fprm"), POFilterTest.class, "TestMonoLingual_FR.po", null, 
					"UTF-8", locEN, locFR, true, true)
			);
	}
	
	@Test // Added/Missing code exception
	public void testPoFilter2 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new POFilter();
		run(testFilter(filter, POFilterTest.class, "AllCasesTest.po", null, 
						"UTF-8", locEN, locFR, true, true));
	}
	
	@Test // Added/Missing code exception
	public void testPoFilter3 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new POFilter();
		run(testFilter(filter, POFilterTest.class, "Test_nautilus.af.po", null, 
						"UTF-8", locEN, locFR, true, true));
	}
		
	@Test
	public void listEvents_POFilter () throws URISyntaxException {
		IFilter filter = new POFilter();
        
        listEvents(filter, POFilterTest.class, "UTF-8", locEN, locFR, "AllCasesTest.po");
	}
	
	@Test
	public void listEvents_POFilter3 () throws URISyntaxException {
		IFilter filter = new POFilter();
        
        listEvents(filter, POFilterTest.class, "UTF-8", locEN, locFR, "Test_nautilus.af.po");
	}
	
	@Test
	public void listEvents_HtmlFilter () throws URISyntaxException {
		IFilter filter = new HtmlFilter();
        
        listEvents(filter, HtmlSnippetsTest.class, "UTF-8", locEN, locFR, "burlington_ufo_center.html");
	}
	
	@Test
	public void listEvents_HtmlFilter2 () throws URISyntaxException {
		IFilter filter = new HtmlFilter();
        
        listEvents(filter, HtmlSnippetsTest.class, "UTF-8", locEN, locFR, "form.html");
	}
	
	@Test
	public void listEvents_TsFilter () throws URISyntaxException {
		IFilter filter = new TsFilter();
        
        listEvents(filter, TsFilterTest.class, "UTF-8", locEN, locFR, "TSTest01.ts");
	}
	
	@Test
	public void listEvents_TsFilter3 () throws URISyntaxException {
		IFilter filter = new TsFilter();
        
        listEvents(filter, TsFilterTest.class, "UTF-8", locEN, locFR, "Test_nautilus.af.ts");
	}
	
	@Test
	public void testPropertiesFilter () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new PropertiesFilter();
		run(testFilter(filter, PropertiesFilterTest.class, "issue_216.properties", null, 
				"UTF-8", locEN, locFR),
			testFilter(loadFilter(filter, "/okf_properties@Test02.fprm"), PropertiesFilterTest.class, "Test02.properties", null, 
					"UTF-8", locEN, locFR, true, true),
			testFilter(loadFilter(filter, "/okf_properties@Test03.fprm"), PropertiesFilterTest.class, "Test03.properties", null, 
					"UTF-8", locEN, locFR, true, true),
			testFilter(loadFilter(filter, "/okf_properties@Test04.fprm"), PropertiesFilterTest.class, "Test04.properties", null, 
					"UTF-8", locEN, locFR, true, true)
				);
	}
	
	@Test
	public void testRegexFilter () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new RegexFilter();
		run(testFilter(filter, RegexFilterTest.class, "TestRules01.txt", null, 
				"UTF-8", locEN, locFR),
			testFilter(filter, RegexFilterTest.class, "Test01_srt_en.srt", "/okf_regex@SRT.fprm", 
					"UTF-8", locEN, locFR, true, true),
			testFilter(filter, RegexFilterTest.class, "Test01_stringinfo_en.info", "/okf_regex@StringInfo.fprm", 
					"UTF-8", locEN, locFR, true, true),
			testFilter(filter, RegexFilterTest.class, "Test01_stringinfo_en.info", "/okf_regex@StringInfo.fprm", 
					"UTF-8", locEN, locFR, true, true),
			testFilter(filter, RegexFilterTest.class, "TestRules01.txt", "/okf_regex@TestRules01.fprm", 
					"UTF-8", locEN, locFR, true, true),
			testFilter(filter, RegexFilterTest.class, "TestRules02.txt", "/okf_regex@TestRules02.fprm", 
					"UTF-8", locEN, locFR, true, true),
			testFilter(filter, RegexFilterTest.class, "TestRules03.txt", "/okf_regex@TestRules03.fprm", 
					"UTF-8", locEN, locFR, true, true),
			testFilter(filter, RegexFilterTest.class, "TestRules04.txt", "/okf_regex@TestRules04.fprm", 
					"UTF-8", locEN, locFR, true, true),
			testFilter(filter, RegexFilterTest.class, "TestRules05.txt", "/okf_regex@TestRules05.fprm", 
					"UTF-8", locEN, locFR, true, true),
			testFilter(filter, RegexFilterTest.class, "TestRules06.txt", "/okf_regex@TestRules06.fprm", 
					"UTF-8", locEN, locFR, true, true),
			testFilter(filter, RegexFilterTest.class, "TestFrenchISL.isl", "/okf_regex@INI.fprm", 
					"Windows-1252", locFR, locFRCA, true, true)
		);
	}
	
	@Test
	public void testTableFilter () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		TableFilter filter = new TableFilter();
		
		Parameters params = (Parameters) filter.getActiveParameters();
        CommaSeparatedValuesFilterTest.setDefaults(params);
        
		run(testFilter(filter, TableFilterTest.class, "csv_test1.txt", null,
				"UTF-8", locEN, locFR),
			testFilter(filter, TableFilterTest.class, "test01.catkeys", null,
				"UTF-8", locEN, locFR),
			testFilter(filter, TableFilterTest.class, "CSVTesting01.csv", null,
				"UTF-8", locEN, locFR)
				);
	}	
	
	@Test
	public void listEvents_XliffFilter () throws URISyntaxException {
		IFilter filter = new XLIFFFilter();
        
        listEvents(filter, XLIFFFilterTest.class, "UTF-8", locEN, locFR, "BinUnitTest01.xlf");
	}		
	
	@Test
	public void listEvents_XliffFilter3 () throws URISyntaxException {
		IFilter filter = new XLIFFFilter();
        
        listEvents(filter, XLIFFFilterTest.class, "UTF-8", locEN, locDE, "TS09-12-Test01.xlf");
	}
	
	@Test
	public void listEvents_TableFilter () throws URISyntaxException {
		TableFilter filter = new TableFilter();
		Parameters params = (Parameters) filter.getActiveParameters();
        CommaSeparatedValuesFilterTest.setDefaults(params);
        
        listEvents(filter, TableFilterTest.class, "test01.catkeys");
	}
	
	@Test
	public void testXliffFilter () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new XLIFFFilter();
		run(testFilter(filter, XLIFFFilterTest.class, new String[] {
					"JMP-11-Test01.xlf",
//					"lqiTest.xlf", // Issue 388
					"Manual-12-AltTrans.xlf",
					"PAS-10-Test01.xlf",
					"RB-11-Test01.xlf",
					"RB-12-Test02.xlf",
					"SF-12-Test03.xlf",
					"NSTest01.xlf",
					"BinUnitTest01.xlf",
					"MQ-12-Test01.xlf"
				}, null, "UTF-8", locEN, locFR, true),
			testFilter(filter, XLIFFFilterTest.class, new String[] {
					"Typo3Draft.xlf",
					"Xslt-Test01.xlf",
					"TS09-12-Test01.xlf"
				}, null, "UTF-8", locEN, locDE, true),
			testFilter(filter, XLIFFFilterTest.class, "OnTramTest01.xlf", null, 
					"UTF-8", LocaleId.fromString("de-de"), LocaleId.fromString("en-us"), true, true),
			testFilter(filter, XLIFFFilterTest.class, new String[] {
					"SF-12-Test01.xlf",
					"SF-12-Test02.xlf",
					"test1_es.xlf",
					"test2_es.xlf"
				}, null, "UTF-8", locEN, locES, true)
			);
	}
	
	@Test
	public void testXliffFilter2 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new XLIFFFilter();
		run(testFilter(filter, XLIFFFilterTest.class,
					"Manual-12-AltTrans.xlf"
				, null, "UTF-8", locEN, locFR, true, true)
			);
	}
	
	@Test
	public void testXliffFilter3 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new XLIFFFilter();
		run(testFilter(filter, XLIFFFilterTest.class, new String[] {
				"TS09-12-Test01.xlf"
				}, null, "UTF-8", locEN, locDE, true)
			);
	}
	
	@Test
	public void testXliffFilter4 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new XLIFFFilter();
		run(testFilter(filter, XLIFFFilterTest.class, new String[] {
				"JMP-11-Test01.xlf"
				}, null, "UTF-8", locEN, locDE, true)
			);
	}
	
	@Test
	public void testXliffFilter5 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new XLIFFFilter();
		run(testFilter(filter, XLIFFFilterTest.class, new String[] {
				"test1_es.xlf"
				}, null, "UTF-8", locEN, locDE, true)
			);
	}
	
	
	@Test
	@Ignore("Issue 388")
	public void testXliffFilter6 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new XLIFFFilter();
		run(testFilter(filter, XLIFFFilterTest.class, new String[] {
				"lqiTest.xlf"
				}, null, "UTF-8", locEN, locFR, true)
			);
	}
	
	@Test
	public void testXmlFilter () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new XMLFilter();
		run(testFilter(filter, XMLFilterTest.class, new String[] {
			"test01.xml",
			"test02.xml",
			"test03.xml",
			"test04.xml",
			"test05.xml",
			"test06.xml",
			"LocNote-1.xml",
			"LocNote-2.xml",
			"LocNote-3.xml",
			"LocNote-4.xml",
			"LocNote-5.xml",
			"LocNote-6.xml",
			"TestMultiLang.xml",
			"XRTT-Source1.xml",
			"TestCDATA1.xml",
			"test07.xml",
			"test08_utf8nobom.xml",
			"test09.xml"
		}, null, "UTF-8", locEN, locEN, true),
	testFilter(filter, XMLFilterTest.class, new String[] {
			"AndroidTest1.xml",
			"AndroidTest2.xml",
			"AndroidTest3.xml",
			"JavaProperties.xml",
			"Test01.resx",
			"test_with_placeholders.resx",
			"MozillaRDFTest01.rdf"
		}, new String[] {
			"okf_xml@AndroidStrings.fprm",
			"okf_xml@AndroidStrings.fprm",
			"okf_xml@AndroidStrings.fprm",
			"okf_xml@JavaProperties.fprm",
			"okf_xml@RESX.fprm",
			"okf_xml@RESX.fprm",
			"okf_xml@MozillaRDF.fprm"
		}, "UTF-8", locEN, locDE, true)
	);
	}
	
	@Test
	public void testXmlFilter2 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new XMLFilter();
		run(testFilter(filter, XMLFilterTest.class, new String[] {
			"test01.xml"
		}, null, "UTF-8", locEN, locEN, true)
	);
	}
	
	@Test
	public void testXmlFilter3 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter filter = new XMLFilter();
		run(testFilter(filter, XMLFilterTest.class, new String[] {
			"Test01.resx"
		}, new String[] {
		}, "UTF-8", locEN, locDE, true)
	);
	}
	
	@Test
	public void testXmlStreamFilter () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter xmlStreamFilter = new XmlStreamFilter();
		FilterConfigurationMapper fcMapper = new FilterConfigurationMapper();
		fcMapper.addConfigurations(net.sf.okapi.filters.html.HtmlFilter.class.getName());
		fcMapper.addConfigurations(net.sf.okapi.filters.xmlstream.XmlStreamFilter.class.getName());
		fcMapper.setCustomConfigurationsDirectory(ClassUtil.getResourceParent(CdataSubfilterWithRegexTest.class, "/bookmap-readme.dita"));
        fcMapper.addCustomConfiguration("okf_html@spaces_freemarker_regex");
        fcMapper.addCustomConfiguration("okf_html@spaces_freemarker_no_regex");
        fcMapper.updateCustomConfigurations();
        xmlStreamFilter.setFilterConfigurationMapper(fcMapper);
        
		run(testFilter(loadFilter(xmlStreamFilter, "/okf_xmlstream@freemarker.fprm"), CdataSubfilterWithRegexTest.class, "xml-freemarker.xml", null, 
					"UTF-8", locEN, locEN, true, true),
			testFilter(loadFilter(xmlStreamFilter, "/okf_xmlstream@freemarker_no_regex.fprm"), CdataSubfilterWithRegexTest.class, "xml-freemarker.xml", null, 
					"UTF-8", locEN, locEN, true, true),
			testFilter(loadFilter(xmlStreamFilter, "/okf_html@spaces_freemarker_regex.fprm"), CdataSubfilterWithRegexTest.class, "freemarker.html", null, 
					"UTF-8", locEN, locEN, true, true),
			testFilter(loadFilter(xmlStreamFilter, "dita.yml"), DitaExtractionComparisionTest.class, "bookmap-readme.dita", null, 
					"UTF-8", locEN, locFR),
			testFilter(loadFilter(xmlStreamFilter, "dita.yml"), PIExtractionTest.class, "PI-Problem.xml", null, 
					"UTF-8", locEN, locFR, true, true),
			testFilter(loadFilter(xmlStreamFilter, "dita.yml"), PIExtractionTest.class, "PI-Problem2.dita", null, 
					"UTF-8", locEN, locFR, true, true)
		);
	}
	
	@Test
	public void testXmlStreamFilter2 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter xmlStreamFilter = new XmlStreamFilter();
		FilterConfigurationMapper fcMapper = new FilterConfigurationMapper();
		fcMapper.addConfigurations(net.sf.okapi.filters.html.HtmlFilter.class.getName());
		fcMapper.addConfigurations(net.sf.okapi.filters.xmlstream.XmlStreamFilter.class.getName());
		fcMapper.setCustomConfigurationsDirectory(ClassUtil.getResourceParent(CdataSubfilterWithRegexTest.class, "/bookmap-readme.dita"));
        fcMapper.addCustomConfiguration("okf_html@spaces_freemarker_regex");
        fcMapper.addCustomConfiguration("okf_html@spaces_freemarker_no_regex");
        fcMapper.updateCustomConfigurations();
        xmlStreamFilter.setFilterConfigurationMapper(fcMapper);
        
        run(testFilter(loadFilter(xmlStreamFilter, "dita.yml"), PIExtractionTest.class, "PI-Problem.xml", null, 
				"UTF-8", locEN, locFR, true, true)
		);
	}
	
	@Test
	public void testXmlStreamFilter_PropertyXML () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter xmlStreamFilter = new XmlStreamFilter();
		loadFilter(xmlStreamFilter, "javaPropertiesXml.yml");
		FilterConfigurationMapper fcMapper = new FilterConfigurationMapper();
		fcMapper.addConfigurations(net.sf.okapi.filters.html.HtmlFilter.class.getName());
		fcMapper.addConfigurations(net.sf.okapi.filters.xmlstream.XmlStreamFilter.class.getName());
		fcMapper.setCustomConfigurationsDirectory(ClassUtil.getResourceParent(CdataSubfilterWithRegexTest.class, "/bookmap-readme.dita"));
		fcMapper.addConfigurations("net.sf.okapi.filters.html.HtmlFilter");
        fcMapper.updateCustomConfigurations();
        xmlStreamFilter.setFilterConfigurationMapper(fcMapper);
        
		run(testFilter(xmlStreamFilter, PropertyXmlExtractionComparisionTest.class, "about.xml", null,
				"UTF-8", locEN, locEN)
		);
	}
	
	@Test
	public void testXmlStreamFilter_PropertyXML_2 () throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		IFilter xmlStreamFilter = new XmlStreamFilter();
		loadFilter(xmlStreamFilter, "javaPropertiesXml.yml");
		FilterConfigurationMapper fcMapper = new FilterConfigurationMapper();
		fcMapper.addConfigurations(net.sf.okapi.filters.html.HtmlFilter.class.getName());
		fcMapper.addConfigurations(net.sf.okapi.filters.xmlstream.XmlStreamFilter.class.getName());
		fcMapper.setCustomConfigurationsDirectory(ClassUtil.getResourceParent(CdataSubfilterWithRegexTest.class, "/bookmap-readme.dita"));
		fcMapper.addConfigurations("net.sf.okapi.filters.html.HtmlFilter");
        fcMapper.updateCustomConfigurations();
        xmlStreamFilter.setFilterConfigurationMapper(fcMapper);
        
		run(testFilter(xmlStreamFilter, PropertyXmlExtractionComparisionTest.class, "about.xml", null,
				"UTF-8", locEN, locEN, true, true)
		);
	}

	@Test
	public void listEvents_XmlStreamFilter () throws URISyntaxException {
		IFilter xmlStreamFilter = new XmlStreamFilter();
		FilterConfigurationMapper fcMapper = new FilterConfigurationMapper();
		fcMapper.addConfigurations(net.sf.okapi.filters.html.HtmlFilter.class.getName());
		fcMapper.addConfigurations(net.sf.okapi.filters.xmlstream.XmlStreamFilter.class.getName());
		fcMapper.setCustomConfigurationsDirectory(ClassUtil.getResourceParent(CdataSubfilterWithRegexTest.class, "/bookmap-readme.dita"));
        fcMapper.addCustomConfiguration("okf_html@spaces_freemarker_regex");
        fcMapper.addCustomConfiguration("okf_html@spaces_freemarker_no_regex");
        fcMapper.updateCustomConfigurations();
        xmlStreamFilter.setFilterConfigurationMapper(fcMapper);
        
        listEvents(loadFilter(xmlStreamFilter, "dita.yml"), PIExtractionTest.class, 
				"UTF-8", locEN, locFR, "PI-Problem.xml");
	}
	
	//---------------------------------------
	protected IFilter loadFilter(IFilter filter, String params) {
		if (params == null) return filter;
		
		if (filter instanceof XmlStreamFilter) {
			XmlStreamFilter xmlStreamFilter = (XmlStreamFilter) filter;
			xmlStreamFilter.setParametersFromURL(filter.getClass().getResource(params));
		}
		else {
			filter.getParameters().load(Util.URLtoURI(filter.getClass().getResource(params)), false);
		}		
		return filter;
	}
	
	private void run (boolean... results) {
		boolean res = true;
		for (boolean r : results) {
			res &= r;
		}
		assertTrue(res);
	}

	private List<InputDocument> getInputDocuments(Class<?> testClass, String rootPath, final String resPath, String paramFile, final boolean singleFile) throws URISyntaxException {
		List<InputDocument> list = new ArrayList<InputDocument>();
		JarFile jarFile = null;
		
		if (ClassUtil.isInJar(testClass, resPath)) {
			// JAR
			try {							
				LOGGER.trace("JAR !!!");				
				jarFile = new JarFile(rootPath);
				try {
					final Enumeration<JarEntry> entries = jarFile.entries();
			        while (entries.hasMoreElements()) {
			            final JarEntry entry = entries.nextElement();
			            final String name = entry.getName();
			            boolean accept = singleFile ? name.equalsIgnoreCase(resPath) : name.endsWith(Util.getExtension(resPath));
			            if (accept) {
			            	// Unzip the test file
//			            	LOGGER.trace("Temp for: " + name);
//			            	String temp = FileUtil.createTempFile("~okapiIT_");
//			            	StreamUtil.copy(jarFile.getInputStream(entry), new File(temp));
//			            	LOGGER.trace(temp);
//			            	list.add(new InputDocument(temp, paramFile));
			            	
//			            	list.add(new InputDocument(jarFile.getInputStream(entry), paramFile));
			            	list.add(new InputDocument(
			            			Util.buildPath(rootPath, name),
			            			testClass, resPath,
			            			paramFile));			            	
			            }
			        }
				} finally {
					jarFile.close();
				}
				
			} catch (IOException e) {
				throw new OkapiIOException();
			}			
		}
		else {
			// FS
			for (String fname : new File(rootPath).list(
					new FilenameFilter() {
						@Override
						public boolean accept(File dir, String name) {						
							return singleFile ? name.equalsIgnoreCase(resPath) : name.endsWith(Util.getExtension(resPath));
						}			
					})) {
				LOGGER.trace(Util.buildPath(rootPath, fname));
//				list.add(new InputDocument(Util.buildPath(rootPath, fname), paramFile));
				list.add(new InputDocument(Util.buildPath(rootPath, fname),
						testClass, fname, paramFile));
			}
		}

		return list;
	}
	
	private List<InputDocument> getInputDocuments(Class<?> testClass, String rootPath, final String[] resPaths, final String[] paramFiles) throws URISyntaxException {
		List<InputDocument> list = new ArrayList<InputDocument>();
		final List<String> rPaths = ListUtil.arrayAsList(resPaths);
		JarFile jarFile = null;
		paramFile = null;
		
		if (ClassUtil.isInJar(testClass, resPaths[0])) {			
			// JAR
			try {
				LOGGER.trace("JAR !!!");
//				rootPath = rootPath.substring(0, rootPath.length() - 1); // Remove the trailing "!" after jar name
				jarFile = new JarFile(rootPath);
				try {
					final Enumeration<JarEntry> entries = jarFile.entries();
			        while (entries.hasMoreElements()) {
			            final JarEntry entry = entries.nextElement();
			            final String name = entry.getName();
			            boolean res = rPaths.contains(name);
						if (res) {
							int index = rPaths.indexOf(name);
							if (paramFiles != null && paramFiles.length == rPaths.size()) {
								paramFile = paramFiles[index];
							}
						}
			            boolean accept = res;
			            if (accept) {
			            	// Unzip the test file
//			            	LOGGER.trace("Temp for: " + name);
//			            	String temp = FileUtil.createTempFile("~okapiIT_");
//			            	StreamUtil.copy(jarFile.getInputStream(entry), new File(temp));
//			            	LOGGER.trace(temp);
//			            	list.add(new InputDocument(temp, paramFile));
			            	String fpath = Util.buildPath(rootPath, name);
							LOGGER.trace("added to list " + fpath);
			            	list.add(new InputDocument(fpath, testClass, name, paramFile));
							paramFile = null;
			            }
			        }
				} finally {
					jarFile.close();
				}
				
			} catch (IOException e) {
				throw new OkapiIOException(e);
			}
		}
		else {
			for (String fname : new File(rootPath).list(
					new FilenameFilter() {
						@Override
						public boolean accept(File dir, String name) {
							boolean res = rPaths.contains(name);
							if (res) {
								int index = rPaths.indexOf(name);
								if (paramFiles != null && paramFiles.length == rPaths.size()) {
									paramFile = paramFiles[index];
								}
							}						
							return res;
						}			
					})) {
				String fpath = Util.buildPath(rootPath, fname);
				LOGGER.trace("added to list " + fpath);
//				list.add(new InputDocument(Util.buildPath(rootPath, fname), paramFile));
				list.add(new InputDocument(fpath, testClass, "/" + fname, paramFile));
				paramFile = null;
			}
		}			

		return list;
	}
	
	private boolean zipTestFilter (IFilter filter, Class<?> testClass, String resPath, String paramFile,
			String defaultEncoding, LocaleId srcLoc, LocaleId trgLoc) throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		return zipTestFilter (filter, testClass, resPath, paramFile,
				defaultEncoding, srcLoc, trgLoc, true, false);
	}
	
	private boolean zipTestFilter (IFilter filter, Class<?> testClass, String resPath, String paramFile,
			String defaultEncoding, LocaleId srcLoc, LocaleId trgLoc, boolean includeSkeleton, boolean singleFile) throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		// Extension for the file list is figured out from resPath
		String rootPath = ClassUtil.getResourceParent(testClass, Util.ensureLeadingSeparator(resPath, true));
		return doTestFilter(true, getInputDocuments(testClass, rootPath, resPath, paramFile, singleFile), filter, testClass, 
				paramFile, defaultEncoding, srcLoc, trgLoc, false);
	}
	
	private boolean zipTestFilter (IFilter filter, Class<?> testClass, String[] resPaths, String[] paramFiles,
			String defaultEncoding, LocaleId srcLoc, LocaleId trgLoc, boolean includeSkeleton) throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		String rootPath = ClassUtil.getResourceParent(testClass, Util.ensureLeadingSeparator(resPaths[0], true));
		return doTestFilter(true, getInputDocuments(testClass, rootPath, resPaths, paramFiles), filter, testClass, 
				paramFile, defaultEncoding, srcLoc, trgLoc, false);		
	}
	
	private boolean zipXmlTestFilter (IFilter filter, Class<?> testClass, String resPath, String paramFile,
			LocaleId srcLoc, LocaleId trgLoc, boolean singleFile) throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		// Extension for the file list is figured out from resPath
		String rootPath = ClassUtil.getResourceParent(testClass, Util.ensureLeadingSeparator(resPath, true));
		return doTestFilter(true, getInputDocuments(testClass, rootPath, resPath, paramFile, singleFile), filter, testClass, 
				paramFile, null, srcLoc, trgLoc, true);
	}
	
	private boolean testFilter (IFilter filter, Class<?> testClass, String resPath, String paramFile,
			String defaultEncoding, LocaleId srcLoc, LocaleId trgLoc) throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		return testFilter (filter, testClass, resPath, paramFile,
				defaultEncoding, srcLoc, trgLoc, true, false);
	}
	
	private boolean doTestFilter (boolean isZip, List<InputDocument> inputDocs, IFilter filter, Class<?> testClass, String paramFile,
			String defaultEncoding, LocaleId srcLoc, LocaleId trgLoc, boolean isXML) throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		// Extension for the file list is figured out from resPath
				
		FileCompare fc = null;
		ZipFileCompare zfc = null;
		XMLFileCompare xfc = null;
		ZipXMLFileCompare zxfc = null;
		
		if (isXML) {
			if (isZip)
				zxfc = new ZipXMLFileCompare();
			else
				xfc = new XMLFileCompare();
		}
		else {
			if (isZip)
				zfc = new ZipFileCompare();
			else
				fc = new FileCompare();
		}
		
			
		String rootPath = Util.getDirectoryName(ClassUtil.getTargetPath(this.getClass()));
		boolean res = true;
		for (InputDocument doc : inputDocs) {						
			String inPath = doc.path;
			String fname = Util.getFilename(inPath, true);
			String goldPath = Util.buildPath(rootPath, "gold", fname);
			String outPath = Util.buildPath(rootPath, "out", fname);
			LOGGER.trace("Processing Document: {}\nout: {}\ngold: {}", doc.path, outPath, goldPath);
			Util.createDirectories(goldPath);
			Util.createDirectories(outPath);			
			
			loadFilter(filter, paramFile);
			
			createOutputFile(false, filter, doc.getInStream(), goldPath, defaultEncoding, srcLoc, trgLoc);
			createOutputFile(true, filter, doc.getInStream(), outPath, defaultEncoding, srcLoc, trgLoc);
			
			if (isXML) {
				if (isZip)
					res &= zxfc.compareFiles(outPath, goldPath);
				else
					res &= xfc.compareFilesPerLines(outPath, goldPath);
			}
			else {
				if (isZip)
					res &= zfc.compareFilesPerLines(outPath, goldPath, defaultEncoding, false);
				else
					res &= fc.compareFilesPerLines(outPath, goldPath, defaultEncoding, false, true); // Ignore case for the case of "UTF-8" vs. "utf-8"
			}			
		}
		
		return res;
	}
	
	private boolean testFilter (IFilter filter, Class<?> testClass, String resPath, String paramFile,
			String defaultEncoding, LocaleId srcLoc, LocaleId trgLoc, boolean includeSkeleton, boolean singleFile) throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		// Extension for the file list is figured out from resPath
		String rootPath = ClassUtil.getResourceParent(testClass, Util.ensureLeadingSeparator(resPath, true));
		return doTestFilter(false, getInputDocuments(testClass, rootPath, resPath, paramFile, singleFile), filter, testClass, 
				paramFile, defaultEncoding, srcLoc, trgLoc, false);
	}
	
	@SuppressWarnings("unused")
	private boolean testFilter (IFilter filter, Class<?> testClass, String resPath, String paramFile,
			String defaultEncoding, LocaleId srcLoc, LocaleId trgLoc, boolean includeSkeleton, boolean singleFile, String... excludedFileNames) throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		// Extension for the file list is figured out from resPath
		String rootPath = ClassUtil.getResourceParent(testClass, Util.ensureLeadingSeparator(resPath, true));
		List<InputDocument> list = getInputDocuments(testClass, rootPath, resPath, paramFile, singleFile);
		List<String> exFiles = ListUtil.arrayAsList(excludedFileNames);
		
		Iterator<InputDocument> iter = list.iterator();
		while(iter.hasNext()){
			InputDocument doc = iter.next();
			if (exFiles.contains(Util.getFilename(doc.path, true))) {
				iter.remove();
			}
		}
		return doTestFilter(false, list, filter, testClass, 
				paramFile, defaultEncoding, srcLoc, trgLoc, false);
	}
	
	private boolean testFilter (IFilter filter, Class<?> testClass, String[] resPaths, String[] paramFiles,
			String defaultEncoding, LocaleId srcLoc, LocaleId trgLoc, boolean includeSkeleton) throws InstantiationException, IllegalAccessException, URISyntaxException, FileNotFoundException {
		String rootPath = ClassUtil.getResourceParent(testClass, Util.ensureLeadingSeparator(resPaths[0], true));
		return doTestFilter(false, getInputDocuments(testClass, rootPath, resPaths, paramFiles), filter, testClass, 
				paramFile, defaultEncoding, srcLoc, trgLoc, false);		
	}
	
	private class ExtraSteps extends CompoundStep {

		@Override
		protected void addStepsToList(List<IPipelineStep> list) {
			if (getPipelineSteps() == null) return;
			
			for (IPipelineStep step : getPipelineSteps()) {
				list.add(step);
			}
		}
		
	}

	protected void createOutputFile(boolean useExtraStep, IFilter filter, InputStream inStream, String outPath, String encoding, LocaleId srcLoc, LocaleId trgLoc) {
		if (useExtraStep) {
			// Process filter events with additional steps
			new XPipeline(
					null,
					new XBatch(
							new XBatchItem(
									inStream,
									encoding,
									outPath,
									encoding,
									srcLoc,
									trgLoc)
							),
							
					new RawDocumentToFilterEventsStep(filter),
					new ExtraSteps(),
					new FilterEventsToRawDocumentStep()
			).execute();
		}
		else {
			// Do not process filter events with additional steps
			new XPipeline(
					null,
					new XBatch(
							new XBatchItem(
									inStream,
									encoding,
									outPath,
									encoding,
									srcLoc,
									trgLoc)
							),
							
					new RawDocumentToFilterEventsStep(filter),
//					new TuDpLogger(),
					new FilterEventsToRawDocumentStep()
			).execute();
		}
	}
	
	private String getPath(Class<?> testClass, String resPath) {
		String rootPath = ClassUtil.getResourceParent(testClass, Util.ensureLeadingSeparator(resPath, true));
		String path = null;
		if (ClassUtil.isInJar(testClass, resPath)) {
			// JAR
			JarFile jarFile = null;
			File temp = null;
			try {							
				LOGGER.trace("JAR !!!");				
				jarFile = new JarFile(rootPath);
				try {
					final Enumeration<JarEntry> entries = jarFile.entries();
			        while (entries.hasMoreElements()) {
			            final JarEntry entry = entries.nextElement();
			            final String name = entry.getName();
			            boolean accept = name.equalsIgnoreCase(resPath);
			            if (accept) {
			            	// Unzip the test file
			            	LOGGER.trace("Temp for: " + name);
			            	temp = FileUtil.createTempFile("~okapi-2_");
			            	StreamUtil.copy(jarFile.getInputStream(entry), temp);
			            	LOGGER.trace(temp.getAbsolutePath());
			            	path = temp.getAbsolutePath();
			            }
			        }
				} finally {
					if (jarFile != null) 
						jarFile.close();
				}
				
			} catch (IOException e) {
				if (jarFile != null)
					// eat exception as we want the original exception thrown
					try {jarFile.close();} catch (IOException e1) {}
				if (temp != null) temp.delete();
				throw new OkapiIOException();
			} 
		}
		else {
			// FS
			path = Util.buildPath(rootPath, resPath);
		}
		return path;
	}
	
	protected final void listEvents(IFilter filter, Class<?> testClass, String defaultEncoding, LocaleId srcLoc, LocaleId trgLoc, String resPath) throws URISyntaxException {
		
		new XPipeline(
				null,
				new XBatch(
						new XBatchItem(
								Util.toURI(getPath(testClass, resPath)),
								defaultEncoding,
								srcLoc,
								trgLoc)
						),
						
				new RawDocumentToFilterEventsStep(filter),
//				new TuDpSgLogger(),
				new ExtraSteps()				
		).execute();
	}
	
	protected final void listEvents(IFilter filter, Class<?> testClass, String resPath) throws URISyntaxException {
		new XPipeline(
				null,
				new XBatch(
						new XBatchItem(
								Util.toURI(getPath(testClass, resPath)),
								"UTF-8",
								locEN)
						),
						
				new RawDocumentToFilterEventsStep(filter),
				new ExtraSteps()//,
//				new TuDpSgLogger()
		).execute();
	}
	
	protected final void listZipEvents(IFilter filter, Class<?> testClass, String defaultEncoding, LocaleId srcLoc, LocaleId trgLoc, String resPath) throws URISyntaxException {
		new XPipeline(
				null,
				new XBatch(
						new XBatchItem(
								Util.toURI(getPath(testClass, resPath)),
								defaultEncoding,
								Util.toURI(getPath(testClass, resPath) + ".out"),
								defaultEncoding,
								srcLoc,
								trgLoc)
						),
						
				new RawDocumentToFilterEventsStep(filter),
//				new ExtraSteps(),
				new TuDpSgLogger(),
				// fe2rd is needed for the OpenXML filter writer to process SSD and switch the skel writer, so the RSS create a simplifier for the content
				new FilterEventsToRawDocumentStep()
		).execute();
	}
	
	protected IPipelineStep[] getPipelineSteps() {
		return null;
	}

	
//	@Test
//	public void testPath() throws URISyntaxException, IOException {
//		String rootPath = "file:/C:/Documents%20and%20Settings/Sergei%20Vasiliev/.m2/repository/net/sf/okapi/filters/okapi-filter-html/0.25-SNAPSHOT/okapi-filter-html-0.25-SNAPSHOT-tests.jar";
//		LOGGER.trace(rootPath);
//		LOGGER.trace(new File(rootPath));
//		//new File("classpath:/" + rootPath);
//		File f = new File(rootPath);
//		URI uri = new URI(rootPath);
//		LOGGER.trace(uri.getPath());
////		File f = new File(uri);
//		JarFile jarFile = new JarFile(rootPath);
//		for (String fname : f.list(
//				new FilenameFilter() {
//					@Override
//					public boolean accept(File dir, String name) {
//												
//						return true;
//					}			
//				})) {
//			LOGGER.trace(Util.buildPath(rootPath, fname));
//			paramFile = null;
//		}
//	}
}
