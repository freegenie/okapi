package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.filters.FilterUtil;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.regex.Parameters;
import net.sf.okapi.filters.regex.RegexFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripRegexSkeletonBasedTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private  RegexFilter regexFilter;
	private String[] testFileList;
	private String root;
	private Parameters p;
	
	@Before
	public void setUp() throws Exception {
		URL url = this.getClass().getResource("/regex/okf_regex@StringInfo.fprm");
		regexFilter = (RegexFilter) FilterUtil.createFilter(url);
		testFileList = getRegexFiles();
		URL url2 = this.getClass().getResource("/regex/Test01_stringinfo_en.info");
		root = Util.getDirectoryName(url2.toURI().getPath()) + File.separator;
		p = new Parameters();
		p.load(url.toURI(), false);
	}

	@After
	public void tearDown() throws Exception {
		regexFilter.close();
	}

	@Test
	public void roundTripRegexFiles() throws FileNotFoundException {
		runTest(false);
		runTest(true);
	}
	
	@SuppressWarnings("resource")
    private void runTest(boolean segment) throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);
			String xliff = root+f+".xliff";
			String original = root + f;
			String tkitMerged = root+f+".tkitMerged";
			String merged = root+f+".merged";			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(regexFilter, rd, p);
			if (segment) {
				events = RoundTripUtils.segment(events);
			}
			RoundTripUtils.writeXliff(events, root, xliff);
			RoundTripUtils.tkitMerge(original, xliff, tkitMerged, "okf_regex@StringInfo", root);			
			RoundTripUtils.legacyMerge(original, xliff, merged, "okf_regex@StringInfo", root);
			
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(tkitMerged, merged, rd.getEncoding()));
			
			RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.ENGLISH);
			List<Event> o = FilterTestDriver.getEvents(regexFilter, ord, p);
			List<Event> t = FilterTestDriver.getEvents(regexFilter, trd, p);
			assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, true, true, false, true));	
		}
	}

	private static String[] getRegexFiles() throws URISyntaxException {
		URL url = RoundTripRegexSkeletonBasedTkitsIT.class.getResource("/regex/Test01_stringinfo_en.info");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".info");
			}
		};
		return dir.list(filter);
	}
}
