package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.railsyaml.RailsYamlFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripYamlSkeletonBasedTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private RailsYamlFilter yamlFilter;
	private String[] testFileList;
	private String root;

	@Before
	public void setUp() throws Exception {
		yamlFilter = new RailsYamlFilter();
		testFileList = getYamlFiles();
		URL url = RoundTripYamlSkeletonBasedTkitsIT.class.getResource("/yaml/en.yml");
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
	}

	@After
	public void tearDown() throws Exception {
		yamlFilter.close();
	}
	
	@Test
	public void roundTripYamlFiles() throws FileNotFoundException {
		runTest(false);
		runTest(true);
	}
	
	@SuppressWarnings("resource")
    private void runTest(boolean segment) throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);
			String xliff = root+f+".xliff";
			String original = root + f;
			String tkitMerged = root+f+".tkitMerged";
			String merged = root+f+".merged";			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(yamlFilter, rd, null);
			if (segment) {
				events = RoundTripUtils.segment(events);
			}
			RoundTripUtils.writeXliff(events, root, xliff);
			String configId = yamlFilter.getConfigurations().get(0).configId;
	        RoundTripUtils.tkitMerge(original, xliff, tkitMerged, configId);
			RoundTripUtils.legacyMerge(original, xliff, merged, configId);
			FileCompare compare = new FileCompare();
			assertTrue(compare.compareFilesPerLines(tkitMerged, merged, rd.getEncoding()));
			// FIXME: Resource ID differences: IResource ID difference: 1='N9B0342F4-dp1' 2='P1EE2F2E8-dp1'
//			RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
//			RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.ENGLISH);
//			List<Event> o = FilterTestDriver.getEvents(yamlFilter, ord, null);
//			List<Event> t = FilterTestDriver.getEvents(yamlFilter, trd, null);
//			assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, true));
		}
	}

	private static String[] getYamlFiles() throws URISyntaxException {
		URL url = RoundTripYamlSkeletonBasedTkitsIT.class.getResource("/yaml/en.yml");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".yml");
			}
		};
		return dir.list(filter);
	}
}
