package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.FileCompare;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.transtable.TransTableFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripTranstableSkeletonBasedTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private TransTableFilter filter;
	private String[] testFileList;
	private String root;

	@Before
	public void setUp() throws Exception {
		filter = new TransTableFilter();
		testFileList = getFiles();
		URL url = RoundTripTranstableSkeletonBasedTkitsIT.class.getResource("/transtable/test01.xml.txt");
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
	}

	@After
	public void tearDown() throws Exception {
		filter.close();
	}

	@Test
	public void roundTripTranstableFiles() throws FileNotFoundException {
		runTest();
	}
	
	@SuppressWarnings("resource")
    private void runTest() throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);
			String xliff = root+f+".xliff";
			String original = root + f;
			String tkitMerged = root+f+".tkitMerged";
			String merged = root+f+".merged";			
			RawDocument rd = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(filter, rd, null);
			RoundTripUtils.writeXliff(events, root, xliff);
			String configId = filter.getConfigurations().get(0).configId;
	        RoundTripUtils.tkitMerge(original, xliff, tkitMerged, configId);
			RoundTripUtils.legacyMerge(original, xliff, merged, configId);
			FileCompare compare = new FileCompare();
			
			assertTrue(compare.compareFilesPerLines(tkitMerged, merged, rd.getEncoding()));
	        // FIXME: get unexpected header exception, files not writen out correctly			
//			RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
//			RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.ENGLISH);
//			List<Event> o = FilterTestDriver.getEvents(filter, ord, null);
//			List<Event> t = FilterTestDriver.getEvents(filter, trd, null);
//			assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, true));				
		}
	}

	private static String[] getFiles() throws URISyntaxException {
		URL url = RoundTripTranstableSkeletonBasedTkitsIT.class.getResource("/transtable/test01.xml.txt");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".txt");
			}
		};
		return dir.list(filter);
	}
}
