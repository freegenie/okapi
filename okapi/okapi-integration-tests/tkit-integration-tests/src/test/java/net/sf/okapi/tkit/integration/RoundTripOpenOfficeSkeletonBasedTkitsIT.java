package net.sf.okapi.tkit.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.ZipFileCompare;
import net.sf.okapi.common.filters.FilterTestDriver;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.filters.openoffice.OpenOfficeFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripOpenOfficeSkeletonBasedTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private OpenOfficeFilter openOfficeFilter;
	private String[] testFileList;
	private String root;

	@Before
	public void setUp() throws Exception {
		openOfficeFilter = new OpenOfficeFilter();
		testFileList = getOpenXmlFiles();
		URL url = RoundTripOpenOfficeSkeletonBasedTkitsIT.class.getResource("/openoffice/TestDocument01.odt");
		root = Util.getDirectoryName(url.toURI().getPath()) + File.separator;
	}

	@After
	public void tearDown() throws Exception {
		openOfficeFilter.close();
	}

	@Test
	public void roundTripOpenOffice() throws FileNotFoundException {
		runTest(false);
		runTest(true);
	}
	
	@SuppressWarnings("resource")
    private void runTest(boolean segment) throws FileNotFoundException {
		for (String f : testFileList) {
			LOGGER.trace(f);
			String xliff = root+f+".xliff";
			String original = root + f;
			String tkitMerged = root+f+".tkitMerged";
			String merged = root+f+".merged";
			RawDocument rd = new RawDocument(Util.toURI(original),
					"UTF-8", LocaleId.ENGLISH);
			List<Event> events = FilterTestDriver.getEvents(openOfficeFilter, rd, null);
			if (segment) {
				events = RoundTripUtils.segment(events);
			}
			RoundTripUtils.writeXliff(events, root, xliff);
			String configId = openOfficeFilter.getConfigurations().get(0).configId;
	        RoundTripUtils.tkitMerge(original, xliff, tkitMerged, configId);
			RoundTripUtils.legacyMerge(original, xliff, merged, configId);
			ZipFileCompare compare = new ZipFileCompare(); 
			assertTrue(compare.compareFilesPerLines(merged, tkitMerged, rd.getEncoding()));
			
			RawDocument ord = new RawDocument(Util.toURI(original), "UTF-8", LocaleId.ENGLISH);
			RawDocument trd = new RawDocument(Util.toURI(tkitMerged), "UTF-8", LocaleId.ENGLISH);
			List<Event> o = FilterTestDriver.getEvents(openOfficeFilter, ord, null);
			List<Event> t = FilterTestDriver.getEvents(openOfficeFilter, trd, null);
			assertTrue("Compare original and merged: " + f, FilterTestDriver.compareEvents(o, t, true, true, false, true));	
		}

	}

	private static String[] getOpenXmlFiles() throws URISyntaxException {
		// read all files in the test xml directory
		URL url = RoundTripOpenOfficeSkeletonBasedTkitsIT.class.getResource("/openoffice/TestDocument01.odt");
		File dir = new File(url.toURI()).getParentFile();

		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".odt") || name.endsWith(".odp") || name.endsWith(".ods");
			}
		};
		return dir.list(filter);
	}
}
