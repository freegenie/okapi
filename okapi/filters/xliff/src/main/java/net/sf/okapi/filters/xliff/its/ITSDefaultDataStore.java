/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.xliff.its;

import java.util.Collection;
import java.util.HashMap;

/**
 * Simple HashMap for storing ITS references for future use.
 */
public class ITSDefaultDataStore implements IITSDataStore {
	
	private HashMap<String, ITSLQICollection> lqiDataStore;
	private HashMap<String, ITSProvenanceCollection> provDataStore;

	@Override
	public void initialize(String identifier) {
		lqiDataStore = new HashMap<String, ITSLQICollection>();
		provDataStore = new HashMap<String, ITSProvenanceCollection>();
	}

	@Override
	public ITSLQICollection getLQIByURI(String uri) {
		return lqiDataStore.get(uri);
	}

	@Override
	public ITSProvenanceCollection getProvByURI(String uri) {
		return provDataStore.get(uri);
	}

	@Override
	public void save(ITSLQICollection lqi) {
		lqiDataStore.put(lqi.getURI(), lqi);
	}

	@Override
	public void save(ITSProvenanceCollection prov) {
		provDataStore.put(prov.getURI(), prov);
	}

	@Override
	public Collection<String> getStoredLQIURIs() {
		return lqiDataStore.keySet();
	}

	@Override
	public Collection<String> getStoredProvURIs() {
		return provDataStore.keySet();
	}
}