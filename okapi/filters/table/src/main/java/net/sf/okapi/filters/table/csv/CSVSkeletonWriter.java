package net.sf.okapi.filters.table.csv;

import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;

public class CSVSkeletonWriter extends GenericSkeletonWriter {

	@Override
	public String processTextUnit(ITextUnit tu) {
		if (tu.hasProperty(CommaSeparatedValuesFilter.PROP_QUALIFIED) && 
			"yes".equals(tu.getProperty(CommaSeparatedValuesFilter.PROP_QUALIFIED).getValue())) {
				return super.processTextUnit(tu);
		}

		TextContainer tc;
		boolean isTarget = tu.hasTarget(outputLoc);
		if (isTarget) {
			tc = tu.getTarget(outputLoc);
		}
		else {
			tc = tu.getSource();
		}
		return super.processTextUnit(tu);
	}

}
