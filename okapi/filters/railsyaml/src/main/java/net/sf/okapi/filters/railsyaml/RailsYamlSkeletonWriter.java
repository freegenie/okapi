/*===========================================================================
  Copyright (C) 2008-2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.filters.railsyaml;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextUnitUtil;
import net.sf.okapi.common.skeleton.GenericSkeletonWriter;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.DumperOptions.ScalarStyle;
import org.yaml.snakeyaml.Yaml;

public class RailsYamlSkeletonWriter extends GenericSkeletonWriter {
	private Yaml yamlWriter;
	private DumperOptions options;
	private boolean escapeNonAscii;
	private Property quote;

	public RailsYamlSkeletonWriter(boolean escapeNonAscii) {
		super();
		this.escapeNonAscii = escapeNonAscii;
	}

	@Override
	public String processTextUnit(ITextUnit resource) {
		// save quote type for fragment processing below
		quote = resource.getProperty(RailsYamlFilter.YAML_STRING_TYPE);
		TextUnitUtil.unsegmentTU(resource);
		return super.processTextUnit(resource);
	}

	/**
	 * Encode yaml strings based on their previous scalar type:
	 * PLAIN, DOUBLE_QUOTE or SINGLE_QUOTE
	 * 
	 * SnakeYaml should handle these cases:
	 * 	Escape codes:
	 *	Numeric   : { "\x12": 8-bit, "\u1234": 16-bit, "\U00102030": 32-bit }
	 *	Protective: { "\\": '\', "\"": '"', "\ ": ' ', "\<TAB>": TAB }
	 *	C         : { "\0": NUL, "\a": BEL, "\b": BS, "\f": FF, "\n": LF, "\r": CR,
	 *		               "\t": TAB, "\v": VTAB }
	 *	Additional: { "\e": ESC, "\_": NBSP, "\N": NEL, "\L": LS, "\P": PS }
	 */
	@Override
	public String getContent(TextFragment tf, LocaleId locToUse,
			EncoderContext context) {
		String unencoded = super.getContent(tf, locToUse, context);

		// can't change DumperOptions after Yaml construction!!
		options = new DumperOptions();
		options.setAllowUnicode(!escapeNonAscii);
		options.setPrettyFlow(false);
		options.setWidth(Integer.MAX_VALUE);

		String q = quote.getValue();
		if (q.equals("\"")) {
			options.setDefaultScalarStyle(ScalarStyle.DOUBLE_QUOTED);
		} else if (q.equals("'")) {
			options.setDefaultScalarStyle(ScalarStyle.SINGLE_QUOTED);
		} else {
			options.setDefaultScalarStyle(ScalarStyle.PLAIN);
		}
		yamlWriter = new Yaml(options);
		String encodedString = yamlWriter.dump(unencoded);

		return encodedString;
	}
}
