/*===========================================================================
 This code from the project: http://jtmt.sourceforge.net/
 -----------------------------------------------------------------------------
 This library is free software; you can redistribute it and/or modify it 
 under the terms of the GNU Lesser General Public License as published by 
 the Free Software Foundation; either version 2.1 of the License, or (at 
 your option) any later version.

 This library is distributed in the hope that it will be useful, but 
 WITHOUT ANY WARRANTY; without even the implied warranty of 
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License 
 along with this library; if not, write to the Free Software Foundation, 
 Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
 ===========================================================================*/

package net.sf.okapi.connectors.moses;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.StreamUtil;
import net.sf.okapi.common.Util;

import com.ibm.icu.text.BreakIterator;
import com.ibm.icu.text.RuleBasedBreakIterator;

public class SimpleTokenizer {
	private RuleBasedBreakIterator wordIterator = null;

	public SimpleTokenizer(LocaleId locale) {	
		wordIterator = new RuleBasedBreakIterator(StreamUtil.streamAsString(
				SimpleTokenizer.class.getResourceAsStream("/word_break_rules.txt"), "UTF-8"));
	}

	public String tokenize(String text) {
		if (Util.isEmpty(text)) {
			return text;
		}
		
		int index = 0;
		wordIterator.setText(text);
		StringBuffer b = new StringBuffer(text.length());
		for (;;) {
			int end = wordIterator.next();
			if (end == BreakIterator.DONE) {
				break;
			}
			b.append(text.substring(index, end) + " "); 			
			index = end;
		}
		// remove last whitespace
		b.setLength(b.length()-1);
		// remove double\triple white space
		String t = b.toString();
		return t.replaceAll("\\s+", " ");
	}
}
