package net.sf.okapi.lib.beans.v2;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.filters.idml.IDMLFilterWriter;
import net.sf.okapi.lib.beans.v1.LocaleIdBean;
import net.sf.okapi.lib.persistence.IPersistenceSession;
import net.sf.okapi.lib.persistence.PersistenceBean;

public class IDMLFilterWriterBean extends PersistenceBean<IDMLFilterWriter> {

	private LocaleIdBean trgLoc = new LocaleIdBean();
	
	@Override
	protected IDMLFilterWriter createObject(IPersistenceSession session) {
		return new IDMLFilterWriter();
	}

	@Override
	protected void setObject(IDMLFilterWriter obj, IPersistenceSession session) {
		LocaleId locId = trgLoc.get(LocaleId.class, session);
		obj.setOptions(locId, null);
	}

	@Override
	protected void fromObject(IDMLFilterWriter obj, IPersistenceSession session) {
		trgLoc.set(obj.getTargetLocale(), session);		
	}

	public LocaleIdBean getTrgLoc() {
		return trgLoc;
	}

	public void setTrgLoc(LocaleIdBean trgLoc) {
		this.trgLoc = trgLoc;
	}

}
