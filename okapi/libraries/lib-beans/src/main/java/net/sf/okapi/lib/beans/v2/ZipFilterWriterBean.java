/*===========================================================================
  Copyright (C) 2008-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.beans.v2;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.filterwriter.ZipFilterWriter;
import net.sf.okapi.lib.beans.v1.LocaleIdBean;
import net.sf.okapi.lib.persistence.IPersistenceSession;
import net.sf.okapi.lib.persistence.PersistenceBean;

public class ZipFilterWriterBean extends PersistenceBean<ZipFilterWriter> {

	private LocaleIdBean locale = new LocaleIdBean();
	private EncoderManagerBean encoderManager = new EncoderManagerBean();
	
	@Override
	protected ZipFilterWriter createObject(IPersistenceSession session) {
		return new ZipFilterWriter(encoderManager.get(EncoderManager.class, session));
	}

	@Override
	protected void fromObject(ZipFilterWriter obj, IPersistenceSession session) {
		locale.set(obj.getLocale(), session);
		encoderManager.set(obj.getEncoderManager(), session);
	}

	@Override
	protected void setObject(ZipFilterWriter obj, IPersistenceSession session) {
		obj.setOptions(locale.get(LocaleId.class, session), null);
	}
	
	public final LocaleIdBean getLocale() {
		return locale;
	}

	public final void setLocale(LocaleIdBean locale) {
		this.locale = locale;
	}

	public final EncoderManagerBean getEncoderManager() {
		return encoderManager;
	}

	public final void setEncoderManager(EncoderManagerBean encoderManager) {
		this.encoderManager = encoderManager;
	}
}
